function readFile(input) {

    if (input.files && input.files[0]) {

        var reader = new FileReader();

        reader.onload = function (e) {

            var filePreview = document.createElement('img');

            filePreview.id = 'file-preview1';

            //e.target.result contents the base64 data from the image uploaded

            filePreview.src = e.target.result;

            console.log(e.target.result);

            var previewZone = document.getElementById('avatar-preview');

            previewZone.appendChild(filePreview);

        }

        reader.readAsDataURL(input.files[0]);

    }

}

function readFile2(input) {

    if (input.files && input.files[0]) {

        var reader = new FileReader();

        reader.onload = function (e) {

            var filePreview = document.createElement('img');

            filePreview.id = 'file-preview2';

            //e.target.result contents the base64 data from the image uploaded

            filePreview.src = e.target.result;

            console.log(e.target.result);

            var previewZone = document.getElementById('banner-preview');

            previewZone.appendChild(filePreview);

        }

        reader.readAsDataURL(input.files[0]);

    }

}

var fileUpload = document.getElementById('file-avatar');
var fileUpload2 = document.getElementById('file-banner');

fileUpload.onchange = function (e) {

    readFile(e.srcElement);

}

fileUpload2.onchange = function (e) {

    readFile2(e.srcElement);

}

function registrarse(){
    var bandera = false;
    if (document.getElementById('name').value.length == 0){
        alert("Llene el campo de nombre")
        bandera = true;
    }
    else if (document.getElementById('username').value.length == 0) {
        alert("Llene el campo de nombre de usuario")
        bandera = true;
    }
    else if (document.getElementById('password').value.length == 0) {
        alert("Llene el campo de contraseña")
        bandera = true;
    }

    if(!bandera){
        window.location.href = "../inicio/logIn.html";
    }

}