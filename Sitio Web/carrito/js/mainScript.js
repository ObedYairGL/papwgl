function logOut() {
    location.href = "../inicio/logIn.html";
}

function historial() {
    location.href = "../historial/record.html";
}

var inputUnits = document.getElementById('units');
var units = parseInt(inputUnits.value);
var spanTotal = document.getElementById('total');
var price = parseInt(document.getElementById('price').textContent);

function calculateTotal() {
    var total = units * price;
    return total;
}

function addUnits(){
    units += 1;
    inputUnits.value = units;
    spanTotal.textContent = calculateTotal();
}

function removeUnits(){
    if(units > 1){
        units -= 1;
        inputUnits.value = units;
    }
    spanTotal.textContent = calculateTotal();
}

window.onload = function () {
    spanTotal.textContent = calculateTotal();
}