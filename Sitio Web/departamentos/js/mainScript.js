function ocultarMostrar() {

    var x = document.getElementById("select-index").selectedIndex;
    var y = document.getElementById("select-index").options;

    if(y[x].text == 'Rango de Fechas'){
        
        var rango1 = document.getElementById("value");
        rango1.type = 'date';
        var rango2 = document.getElementById("rango2");
        rango2.style.opacity = 1;
        rango2.disabled = false;
        rango2.required = true;
    } 
    else {
        var rango1 = document.getElementById("value");
        rango1.type = 'text';
        var rango2 = document.getElementById("rango2");
        rango2.style.opacity = 0;
        rango2.disabled = true;
        rango2.required = false;
    }

}

var selectValue = document.getElementById('select-index');

selectValue.onchange = function (e) {
    ocultarMostrar()
}

function logOut() {
    location.href = "../inicio/logIn.html";
}

function historial() {
    location.href = "../historial/record.html";
}

function kart() {
    location.href = "../carrito/kart.html";
}