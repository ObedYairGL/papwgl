/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tarea10;
import java.lang.*;

/**
 *
 * @author ObedBlacks
 */
import java.util.Scanner;

public class Tarea10 {
    
    static int opcion, opcionMaquina;
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        piedraPapelTijeras();
    }
    
    public static void piedraPapelTijeras(){
        while(true){
            System.out.println("Elije: ");
            System.out.println("1. Piedra\n2. Papel\n3. Tijeras");
            Scanner scanner = new Scanner(System.in);
            opcion = scanner.nextInt();
            scanner.nextLine();
            opcionMaquina = Math.round((int) (Math.random() * 3)) + 1;
            
            System.out.println("El jugador ha elegido: " + opcion);
            System.out.println("La maquina ha elegido: " + opcionMaquina);
            System.out.print("Resultado: ");

            if(opcion == 1){
                switch (opcionMaquina) {
                    case 1:
                        System.out.println("Empate");
                        break;
                    case 2:
                        System.out.println("Has perdido");
                        break;
                    case 3:
                        System.out.println("Has ganado");
                        break;
                    default:
                        break;
                }
            }

            if(opcion == 2){
                switch (opcionMaquina) {
                    case 1:
                        System.out.println("Has ganado");
                        break;
                    case 2:
                        System.out.println("Empate");
                        break;
                    case 3:
                        System.out.println("Has perdido");
                        break;
                    default:
                        break;
                }
            }

            if(opcion == 3){
                switch (opcionMaquina) {
                    case 1:
                        System.out.println("Has perdido");
                        break;
                    case 2:
                        System.out.println("Has ganado");
                        break;
                    case 3:
                        System.out.println("Empate");
                        break;
                    default:
                        break;
                }
            }
        }
    }
}